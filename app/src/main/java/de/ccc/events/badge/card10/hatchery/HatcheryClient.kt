/*
 * Copyright by the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.ccc.events.badge.card10.hatchery

import android.util.Log
import de.ccc.events.badge.card10.HATCHERY_BASE_URL
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.InputStream

private const val TAG = "HatcheryClient"

class HatcheryClient {
    fun getAppList(): List<App> {
        val client = OkHttpClient()

        // TODO: Filter by category
        val request = Request.Builder()
            .url("$HATCHERY_BASE_URL/eggs/list/json")
            .build()

        val response: Response
        try {
            response = client.newCall(request).execute()
        } catch (e: Exception) {
            throw HatcheryClientException(0)
        }

        if (response.code != 200) {
            throw HatcheryClientException(response.code)
        }

        val body = response.body?.string() ?: ""
        val resultList = mutableListOf<App>()

        try {
            val responseJson = JSONArray(body)

            for (i in 0 until responseJson.length()) {
                val item = responseJson.getJSONObject(i)
                resultList.add(
                    App(
                        name = item.getString("name"),
                        slug = item.getString("slug"),
                        description = item.getString("description"),
                        download_counter = item.getInt("download_counter"),
                        status = item.getString("status"),
                        revision = item.getString("revision"),
                        size_of_zip = item.getInt("size_of_zip"),
                        size_of_content = item.getInt("size_of_content"),
                        category = item.getString("category")
                    )
                )
            }
        } catch (e: JSONException) {
            Log.e(TAG, "Error parsing JSON: ${e.message}")
            throw HatcheryClientException(0)
        }

        return resultList
    }

    fun getReleaseUrl(app: App): String {
        val client = OkHttpClient()
        val request = Request.Builder()
            .url("$HATCHERY_BASE_URL/eggs/get/${app.slug}/json")
            .build()

        val response: Response
        try {
            response = client.newCall(request).execute()
        } catch (e: Exception) {
            throw HatcheryClientException(0)
        }

        if (response.code != 200) {
            throw HatcheryClientException(response.code)
        }

        val body = response.body?.string() ?: ""

        try {
            val responseJson = JSONObject(body)
            val version = responseJson.getJSONObject("info").getString("version")
            return responseJson.getJSONObject("releases")
                .getJSONArray(version).getJSONObject(0).getString("url")

        } catch (e: JSONException) {
            Log.e(TAG, "Error parsing JSON: ${e.message}")
            throw HatcheryClientException(0)
        }
    }

    fun openDownloadStream(app: App): InputStream {
        val releaseUrl = getReleaseUrl(app)

        val client = OkHttpClient()
        val request = Request.Builder()
            .url(releaseUrl)
            .build()

        val response: Response
        try {
            response = client.newCall(request).execute()
        } catch (e: Exception) {
            throw HatcheryClientException(0)
        }

        if (response.code != 200) {
            throw HatcheryClientException(response.code)
        }

        return response.body?.byteStream() ?: throw HatcheryClientException(0)
    }
}
