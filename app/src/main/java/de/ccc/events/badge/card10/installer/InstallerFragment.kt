/*
 * Copyright by the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.ccc.events.badge.card10.installer;

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.UiThread
import androidx.annotation.WorkerThread
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import de.ccc.events.badge.card10.R
import kotlinx.android.synthetic.main.installer_fragment.*
import okhttp3.*
import okhttp3.HttpUrl.Companion.toHttpUrl
import java.io.IOException

class InstallerFragment : Fragment(), Callback {
    val viewModel: InstallerViewModel by lazy { ViewModelProviders.of(this).get(InstallerViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.url.observe(
            this,
            Observer { url -> installer_fragment_url.text = "Installing card10 app from ${url}" })
        viewModel.appBytes.observe(
            this,
            Observer { bytes -> installer_fragment_app.text = "${bytes.size} bytes loaded" })
        viewModel.message.observe(this, Observer { message -> installer_fragment_message.text = message })

        val url = activity!!.intent.data.toString().toHttpUrl()
        viewModel.url.value = url
        startDownload(url)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.installer_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    }

    @WorkerThread
    override fun onResponse(call: Call, response: Response) {
        if (response.isSuccessful) {
            activity!!.runOnUiThread {
                downloadSuccessful(response.body!!.bytes())
            }
        } else {
            viewModel.message.postValue("${response.code} ${response.message}")
        }
    }

    @WorkerThread
    override fun onFailure(call: Call, e: IOException) {
        viewModel.message.postValue(e.toString())
    }

    @UiThread
    fun startDownload(url: HttpUrl) {
        val request = Request.Builder()
            .url(url)
            .build()
        val client = OkHttpClient()
        client.newCall(request).enqueue(this)
        viewModel.message.value = "Downloading…"
    }

    @UiThread
    fun downloadSuccessful(appBytes: ByteArray) {
        viewModel.appBytes.value = appBytes
        viewModel.message.value = null
        // TODO upload to card10
    }
}
